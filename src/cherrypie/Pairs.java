/*
 You can use, redistribute, or modify this software under the terms of the
 GNU Affero General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.
 You should have received a copy of the
 GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.
 © 2014 : Nathan Johnson
 */
package cherrypie;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public abstract class Pairs {

    public static final Set<Couple> pairs = new TreeSet<>();
    protected static String[] imageNames = {"input-mouse.png", "audio-headphones.png", "computer.png"};
    protected static Map<String, SwungImage> images = new TreeMap<>();

    static {
        //Initialize own data
        for (String name : imageNames) {
            images.put(name, new SwungImage(name));
        }
        pairs.add(new Couple(Tag.INPUT, images.get("input-mouse.png")));
        pairs.add(new Couple(Tag.OUTPUT, images.get("audio-headphones.png")));
        pairs.add(new Couple(Tag.INPUT, images.get("computer.png")));
        pairs.add(new Couple(Tag.OUTPUT, images.get("computer.png")));
    }
}
