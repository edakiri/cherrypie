/*
 You can use, redistribute, or modify this software under the terms of the
 GNU Affero General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.
 You should have received a copy of the
 GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.
 © 2014 : Nathan Johnson
 */
package cherrypie;

import javax.swing.JCheckBox;

public class ComparableCheckBox extends JCheckBox implements Comparable {

    public int compareTo(Object o) {
        return toString().compareTo(o.toString());
    }

    ComparableCheckBox(String string) {
        super(string);
    }

}
