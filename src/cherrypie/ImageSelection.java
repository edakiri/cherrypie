/*
 You can use, redistribute, or modify this software under the terms of the
 GNU Affero General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.
 You should have received a copy of the
 GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.
 © 2014 : Nathan Johnson
 */
package cherrypie;

import java.awt.Container;
import java.util.Set;
import java.util.TreeSet;
import javax.swing.JPanel;
import javax.swing.JFrame;

public class ImageSelection extends JPanel {

    Set<SwungImage> set = new TreeSet<>();

    public void add(Set<SwungImage> changes) {
        for (SwungImage image : changes) {
            System.out.println(image.fileName + " was " + image.linkCount);
            assert image.linkCount >= 0;
            image.linkCount++;
            if (!set.contains(image)) {
                set.add(image);
                super.add(image);
            }
        }

        //repack
        Container parent = getParent();
        while (parent.getParent() != null) {
            parent = parent.getParent();
        }
        ((JFrame) parent).pack();
    }

    public void remove(Set<SwungImage> changes) {
        for (SwungImage image : changes) {
            System.out.println(image.fileName + " was " + image.linkCount);
            image.linkCount--;
            assert image.linkCount >= 0;
            if (set.contains(image)) {
                if (image.linkCount == 0) {
                    set.remove(image);
                    super.remove(image);
                }
            }
        }

        //repack
        Container parent = getParent();
        while (parent.getParent() != null) {
            parent = parent.getParent();
        }
        ((JFrame) parent).pack();
    }
}
