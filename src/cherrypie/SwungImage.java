/*
 You can use, redistribute, or modify this software under the terms of the
 GNU Affero General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.
 You should have received a copy of the
 GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.
 © 2014 : Nathan Johnson
 */
package cherrypie;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class SwungImage extends java.awt.Component implements Comparable {

    BufferedImage img;
    public final String fileName;
    public int linkCount;

    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }

    public SwungImage(String fileName) {
        this.fileName = fileName;
        try {
            System.out.println("Creating image " + fileName);
            File file = new File(fileName);
            img = ImageIO.read(file);
            System.out.println("done");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Dimension getPreferredSize() {
        return new Dimension(img.getWidth(null), img.getHeight(null));
    }

    @Override
    public int compareTo(Object o) {
        SwungImage oi = (SwungImage) o;
        return fileName.compareTo(oi.fileName);
    }
}
