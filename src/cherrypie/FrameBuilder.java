/*
 You can use, redistribute, or modify this software under the terms of the
 GNU Affero General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.
 You should have received a copy of the
 GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.
 © 2014 : Nathan Johnson
 */
package cherrypie;

import java.awt.Container;
import javax.swing.*;

public class FrameBuilder implements Runnable {

    private Container contentPane;

    public void run() {
        //Initialize GUI
        ImageSelection image = new ImageSelection();
        JFrame frame = new JFrame("Pie");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
        JLabel imageTag = new JLabel("Images and Tags");
        contentPane = frame.getContentPane();
        BoxLayout layout = new BoxLayout(contentPane, BoxLayout.PAGE_AXIS);
        contentPane.setLayout(layout);
        contentPane.add(imageTag);
        contentPane.add(new TagSelection(image));
        contentPane.add(image);
        frame.pack();
    }
}
