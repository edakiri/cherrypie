/*
 You can use, redistribute, or modify this software under the terms of the
 GNU Affero General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.
 You should have received a copy of the
 GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.
 © 2014 : Nathan Johnson
 */
package cherrypie;

public class Couple implements Comparable {

    public final Tag tag;
    public final SwungImage image;

    public Couple(Tag tag, SwungImage image) {
        this.tag = tag;
        this.image = image;
    }

    @Override
    public int compareTo(Object o) {
        Couple oc = (Couple) o;
        int tagCompare = tag.compareTo(oc.tag);
        if (tagCompare != 0) {
            return tagCompare;
        }
        int imageCompare = image.fileName.compareTo(oc.image.fileName);
        return imageCompare;
    }
}
