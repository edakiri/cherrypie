/*
 You can use, redistribute, or modify this software under the terms of the
 GNU Affero General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.
 You should have received a copy of the
 GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.
 © 2014 : Nathan Johnson
 */
package cherrypie;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;
import javax.swing.*;

public class TagSelection extends JPanel implements ItemListener {

    ImageSelection imageSelection;

    public TagSelection(ImageSelection imageSelection) {
        this.imageSelection = imageSelection;
        SortedSet<ComparableCheckBox> set = new TreeSet<>();
        Tag[] v = Tag.values();
        BoxLayout layout = new BoxLayout(this, BoxLayout.PAGE_AXIS);
        setLayout(layout);
        add(new JLabel("Select tags."));

        for (Tag t : v) {
            System.out.println(t);

            ComparableCheckBox check = new ComparableCheckBox(t.toString());
            set.add(check);
            add(check);
            check.addItemListener(this);
        }
    }

    Set<SwungImage> imagesFor(Tag tag) {

        Set<SwungImage> imagesForTag = new TreeSet<>();
        for (Couple couple : Pairs.pairs) {
            if (couple.tag == tag) {
                imagesForTag.add(couple.image);
            }
        }
        return imagesForTag;
    }

    public void itemStateChanged(ItemEvent e) {
        boolean selected = e.getStateChange() == ItemEvent.SELECTED;
        ComparableCheckBox check = (ComparableCheckBox) e.getItemSelectable();
        String string = check.getActionCommand();
        Tag tag = Tag.valueOf(string);
        Set<SwungImage> imagesForTag = imagesFor(tag);
        if (selected) {
            imageSelection.add(imagesForTag);
        } else {
            imageSelection.remove(imagesForTag);
        }
        System.out.println(string + " " + selected);

    }

}
