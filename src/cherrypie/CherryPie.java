/*
 You can use, redistribute, or modify this software under the terms of the
 GNU Affero General Public License as published by the Free Software Foundation,
 either version 3 of the License, or any later version.
 You should have received a copy of the
 GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.
 © 2014 : Nathan Johnson
 */
package cherrypie;

import static javax.swing.SwingUtilities.invokeAndWait;

public class CherryPie {

    /**
     * @param args Arguments are ignored.
     */
    public static void main(String[] args) throws Exception {
        System.out.println(System.getProperty("user.dir"));
        FrameBuilder frameBuilder = new FrameBuilder();
        invokeAndWait(frameBuilder);
    }
}
